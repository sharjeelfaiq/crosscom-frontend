import React from "react";

const Footer = () => {
  return (
    <footer className="fixed bottom-0 left-0 w-full bg-slate-500 h-20 flex justify-center items-center ">
      <h1 className="text-white text-xl font-semibold">CrossCom &copy;</h1>
    </footer>
  );
};

export default Footer;
